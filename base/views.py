"""
chatroom_bot base module views
"""
## @package base.views
#
# Views of base application
# @date 19-07-2019
# @version 1.0
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

    
class Home(LoginRequiredMixin,TemplateView):
    """!
    Initial class

    @version 1.0.0
    """
    template_name = "home.html"
