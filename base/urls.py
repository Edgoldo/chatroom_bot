"""
chatroom_bot base module URL Configuration
"""
## @package base.urls
#
# Link between views and templates
# @date 19-07-2019
# @version 1.0
from django.urls import path
from .views import *

app_name = 'base'
urlpatterns = [
    path('', Home.as_view(), name = "home")
]