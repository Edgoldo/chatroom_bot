"""
chatroom_bot base module functions
"""
## @package base.functions
#
# Base functions for project
# @date 19-07-2019
# @version 1.0
from django.contrib.auth.models import User
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware

    
def validate_email(email):
    """!
    Function to validate mail

    @param email {str} User email
    @return Boolean
    """
    
    email = User.objects.filter(email=email)
    if email:
        return True
    else:
        return False

def setup_request(request):
    """!
    Function to setup request in test cases

    @param request {object} request object
    """
    # Session Middleware
    middleware = SessionMiddleware()
    middleware.process_request(request)
    request.session.save()
    # Message Middleware
    middleware = MessageMiddleware()
    middleware.process_request(request)
    request.session.save()

