function get_comment(room_id,author){
  $.ajax({
    type: 'GET',
    url: URL_LIST_COMMENT,
    data:{
      'room_id':room_id
    },
    success: function(response) {
      var data = JSON.parse(response);
      var html = '';
      $.each(data,function(key,value){
        var send = value.fields.seen ? 'done_all' :'done';
        var comment = value.fields.visible ? value.fields.comment :'This comment was deleted';
        html += '<div class="row" onclick="select_chat(this)" data-comment="'+value.pk+'">';
        if(value.fields.fk_user_emit==author){
          html += '<div class="recived left">'+comment;
        }
        else{
          html += '<div class="sended right">'+comment;
          send = 'done_all';
        }
        if(value.fields.visible){
          html += '<br/><i class="tiny material-icons prefix">'+send+'</i> | ';
          html += '<small>'+ value.fields.fk_user_emit +'</small> |';
          html += '<small>'+ value.fields.date +'</small>';
        }
        html += '</div></div>';
      });
      $('.chat-box').show();
      $('.chat-log').html(html);
      $('.chat-log').scrollTop(9999999)   
      }
  });
}

function sendMessage(author,comment,room_id){
  $.ajax({
    type: 'POST',
    url: URL_CREATE_COMMENT,
    data:{
      "comment":comment,
      "user_emit":author,
      "room_id":room_id,
    },
    success: function(response) {
      var data = JSON.parse(response);
      var fields = data[0].fields;
      $('#id_comment').val('');
      chatSocket.send(JSON.stringify({
        'comment':fields.comment,
        'date':fields.date,
        'fk_user_emit':fields.fk_user_emit,
        'seen':fields.seen
      }));  
    }
  });
}
