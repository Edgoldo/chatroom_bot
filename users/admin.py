"""
chatroom_bot users module Admin Configuration
"""
## @package users.admin
#
# Admin configurations for users module
# @date 19-07-2019
# @version 1.0
from django.contrib import admin
