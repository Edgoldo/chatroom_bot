"""
chatroom_bot users module views
"""
## @package users.views
#
# Views of users application
# @date 19-07-2019
# @version 1.0
from django.shortcuts import redirect
from django.views.generic import (
    FormView, RedirectView, CreateView, 
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth import authenticate, logout, login
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from .forms import (
    LoginForm, UserRegisterForm, PasswordChangeAccount
)
from .constants import SESSION_EXPIRATION


class LoginView(FormView):
    """!
    Class for login form

    @version 1.0.0
    """
    form_class = LoginForm
    template_name = 'user.login.html'
    success_url = reverse_lazy('base:home')

    def form_valid(self, form):
        """!
        Method to validate login form
    
        @param self <b>{object}</b> Class instance
        @param form <b>{object}</b> Form object
        @return Valid Form
        """
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        login(self.request, user)
        if self.request.POST.get('remember_me') is not None:
            self.request.session.set_expiry(SESSION_EXPIRATION)
        return super().form_valid(form)
    
    
class LogoutView(RedirectView):
    """!
    Class for logout

    @version 1.0.0
    """
    permanent = False
    query_string = True

    def get_redirect_url(self):
        """!
        Method for redirect
    
        @param self <b>{object}</b> Class instance
        @return Redirect url
        """
        logout(self.request)
        return reverse_lazy('users:login')


class RegisterView(SuccessMessageMixin,CreateView):
    """!
    Class for register user

    @version 1.0.0
    """
    template_name = "user.register.html"
    form_class = UserRegisterForm
    success_url = reverse_lazy('base:home')
    success_message = "Successfully registered"
    model = User


class ChangePasswordView(LoginRequiredMixin,FormView):
    """!
    Class for manage change password

    @version 1.0.0
    """
    template_name = "change_password.form.html"
    form_class = PasswordChangeAccount
    success_url = reverse_lazy('base:home')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        """!
        Method for validate data
    
        @param self <b>{object}</b> Class instance
        @param form <b>{object}</b> Form object
        @return Return a redirect
        """
        user = form.save()
        try:
            login(self.request, user)
        except:
            return super().form_valid(form)    
        return super().form_valid(form)
