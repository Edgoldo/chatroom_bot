"""
chatroom_bot users module formularies
"""
## @package users.forms
#
# Forms of users application
# @date 19-07-2019
# @version 1.0
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    UserCreationForm
)
from django.forms.fields import (
    CharField, BooleanField
)
from django.forms.widgets import (
    PasswordInput, CheckboxInput
)
from django.contrib.auth.forms import (
    PasswordResetForm, SetPasswordForm, PasswordChangeForm
)
from base.functions import (
    validate_email
)

class LoginForm(forms.Form):
    """!
    Class for login form

    @version 1.0.0
    """
    password = CharField()

    username = CharField()

    remember_me = BooleanField()


    def __init__(self, *args, **kwargs):
        """!
        Metod to override form initial

        @param self <b>{object}</b> Class instance
        @param args <b>{list}</b> Args List
        @param kwargs <b>{dict}</b> Args Dict
        """
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = PasswordInput()
        self.fields['password'].widget.attrs.update({'class': 'validate',
        'placeholder': 'Password'})
        self.fields['username'].widget.attrs.update({'class': 'validate',
        'placeholder': 'Username'})
        self.fields['remember_me'].label = "Remember Me"
        self.fields['remember_me'].widget = CheckboxInput()
        self.fields['remember_me'].required = False

    def clean(self):
        """!
        Method to validate form

        @param self <b>{object}</b> Class instance
        @return Return error on form
        """
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        user = authenticate(username=username,password=password)
        if(not user):
            msg = "Invalid user or password"
            self.add_error('username', msg)

    class Meta:
        fields = ('username', 'password', 'remember_me')


class UserRegisterForm(UserCreationForm):
    """!
    Class to register user

    @version 1.0.0
    """
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2',
                  'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)

        self.fields['username'].required = True
        self.fields['username'].label = 'Username'

        self.fields['password1'].required = True
        self.fields['password1'].label = 'Password'

        self.fields['password2'].required = True
        self.fields['password2'].label = 'Password Repeat'

        self.fields['first_name'].label = 'First Name'

        self.fields['last_name'].label = 'Last Name'

        self.fields['email'].label = 'Email'

    def clean_password_repeat(self):
        """!
        Method to validate passwords

        @param self <b>{object}</b> Class instance
        @return Return field validation
        """
        password = self.cleaned_data['password1']
        password_repeat = self.cleaned_data['password2']
        if(password_repeat!=password):
            raise forms.ValidationError("Password does not match")
        return password_repeat

    def clean_email(self):
        """!
        Method to validate email

        @param self <b>{object}</b> Class instance
        @return Return field with validation
        """
        email = self.cleaned_data['email']
        if(validate_email(email)):
            raise forms.ValidationError("Email exists")
        return email


class PasswordResetForm(PasswordResetForm):
    """!
    Class to reset password

    @version 1.0.0
    """

    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                  'placeholder': 'Email'})

    def clean(self):
        cleaned_data = super(PasswordResetForm, self).clean()
        email = cleaned_data.get("email")

        if email:
            msg = "Email not found"
            try:
                User.objects.get(email=email)
            except:
                self.add_error('email', msg)



class PasswordConfirmForm(SetPasswordForm):
    """!
    Class to password confirm form

    @version 1.0.0
    """
    def __init__(self, *args, **kwargs):
        super(PasswordConfirmForm, self).__init__(*args, **kwargs)
        self.fields['new_password1'].widget.attrs.update({'class': 'input-field',
                                                  'placeholder': 'New Password'})
        self.fields['new_password2'].widget.attrs.update({'class': 'input-field',
                                                  'placeholder': 'Repeat Password'})


class PasswordChangeForms(forms.Form):
    """!
    Class for password change

    @version 1.0.0
    """

    old_password = forms.CharField(max_length=20,
        widget=forms.TextInput(attrs={'type':'password'}),
        label="Old Password"
        )

    new_password = forms.CharField(max_length=20,
        widget=forms.TextInput(attrs={'type':'password'}),
        label="New Password"
        )

    new_password_repeat = forms.CharField(max_length=20,
        widget=forms.TextInput(attrs={'type':'password'}),
        label="Repeat New Password"
        )


class PasswordChangeAccount(PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(PasswordChangeAccount, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs.update({'class': 'input-field',
                                                  'placeholder': 'Old Password'})
        self.fields['new_password1'].widget.attrs.update({'class': 'input-field',
                                                  'placeholder': 'New Password'})
        self.fields['new_password2'].widget.attrs.update({'class': 'input-field',
                                                  'placeholder': 'Repeat New Password'})     