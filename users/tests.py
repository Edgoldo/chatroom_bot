from django.test import TestCase, RequestFactory
from base.functions import setup_request
from .forms import *
from .views import *

class RegisterUserTest(TestCase):
  """!
  Class for test user register

  @version 1.0.0
  """

  def setUp(self):
    """!
    Method to setup test case
    @param self <b>{object}</b> Class instance
    """
    self.factory = RequestFactory()

  def testForm(self):
    """!
    Method to test form with invalid data
    @param self <b>{object}</b> Class instance
    """
    form = UserRegisterForm(data = {
      'username': "test",
      'email': "testmail.com",
      'password1': "123",
      'password2': "123",
      'first_name': "test",
      'last_name': "user"
    })
    self.assertFalse(form.is_valid())
    self.assertIsNotNone(form.errors)

  def testRegister(self):
    """!
    Method to test user register
    @param self <b>{object}</b> Class instance
    """
    user = User.objects.count()
    request = self.factory.post("/register", 
      {'email': "test@mail.com", 'username': "test", 'first_name': "test",
      "last_name":"user","password1": "prueba123", "password2": "prueba123"}) 
    setup_request(request)
    response = RegisterView.as_view()(request)
    self.assertEqual(response.status_code, 302)
    self.assertEqual(User.objects.count(), user+1)
