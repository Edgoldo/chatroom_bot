"""
chatroom_bot chat module models
"""
## @package chat.models
#
# Models of chat application
# @date 19-07-2019
# @version 1.0
from django.contrib.auth.models import User
from django.db import models

class ChatRoom(models.Model):
    """!
    Class for chat room model

    @copyright MIT
    @version 1.0.0
    """
    name = models.CharField(max_length=50)

    def __str__(self):
        """!
        Function to show string

        @return Return chat string
        """
        return self.name

class RoomUsers(models.Model):
    """!
    Class for chat room users

    @copyright MIT
    @version 1.0.0
    """
    user = models.ForeignKey(User, related_name='user_room', on_delete=models.CASCADE)

    room = models.ForeignKey(ChatRoom, related_name='room_room', on_delete=models.CASCADE)

    def __str__(self):
        """!
        Function to show string

        @return Return chat string
        """
        return self.room.name


class Comment(models.Model):
    """!
    Class for comment model

    @copyright MIT
    @version 1.0.0
    """
    fk_user_emit = models.ForeignKey(User, related_name='user_emit', on_delete=models.CASCADE)
    comment = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(default=True)
    seen = models.BooleanField(default=False)
    room = models.ForeignKey(ChatRoom, related_name='chat_room', on_delete=models.CASCADE)

    class Meta:
        """!
            Class for model metadata
        """
        ordering = ('date',)
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'

    def __str__(self):
        """!
        Function to show string

        @return Return comment string
        """
        return self.comment