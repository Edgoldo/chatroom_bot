"""
chatroom_bot chat module URL Configuration
"""
## @package chat.urls
#
# Link between views and templates
# @date 19-07-2019
# @version 1.0
from django.urls import path
from .views import *

app_name = 'chat'
urlpatterns = [
    path('', RoomView.as_view(), name = "chat_list"),
    path('create-room',CreateRoomView.as_view(), name = "room_create"),
    path('<int:room>', ChatView.as_view(), name = "chat"),
    path('list-comment', ListComment.as_view(), name="list_comment"),
    path('comment', AddComment.as_view(), name="comment"),
]