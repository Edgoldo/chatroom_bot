# API address to get the stock quote of the parameter code
STOCK_URL = 'https://stooq.com/q/l/?s={stock_code}&f=sd2t2ohlcv&h&e=csv​'

# Posible invalid values that get when call of STOCK_URL
INVALID_CLOSE_VALUES = ['', 'Close', 'N/D']

# Command to call STOCK_URL
COMMAND_STOCK = '/stock='