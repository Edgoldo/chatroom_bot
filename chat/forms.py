"""
chatroom_bot chat module formularies
"""
## @package chat.forms
#
# Forms of chat application
# @date 19-07-2019
# @version 1.0
from django import forms
from .models import ChatRoom

class ChatSendForm(forms.Form):
  """!
  Class for chat send form

  @version 1.0.0
  """ 

  comment = forms.CharField()


class ChatRegisterForm(forms.ModelForm):
  """!
  Class for chat room form

  @version 1.0.0
  """ 
  class Meta:
    model = ChatRoom
    fields = '__all__'