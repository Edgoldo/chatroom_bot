"""
chatroom_bot chat module routing configuration
"""
## @package chat.routing
#
# Web Socket routing
# @version 1.0
from django.urls import path

from . import consumers

websocket_urlpatterns = [
  path('chat/<int:room>', consumers.ChatConsumer),
]