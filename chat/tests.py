from django.test import TestCase, RequestFactory
from base.functions import setup_request
from .forms import *
from .views import *

class ChatMessageTest(TestCase):
  """!
  Class for test chat message

  @version 1.0.0
  """

  def setUp(self):
    """!
    Method to setup test case
    @param self <b>{object}</b> Class instance
    """
    self.factory = RequestFactory()
    self.room = ChatRoom.objects.create(name='testRoom')
    self.user = User.objects.create_user(
      username='testuser', email='test@mail.com', password='test123')

  def testRegister(self):
    """!
    Method to chat message
    @param self <b>{object}</b> Class instance
    """
    user = User.objects.count()
    request = self.factory.post("/comment", 
      {'comment': "Test comment", 'user_emit': self.user.pk, 'room_id': self.room.pk}) 
    setup_request(request)
    response = AddComment.as_view()(request)
    comment = Comment.objects.first()
    self.assertEqual(response.status_code, 200)
    self.assertEqual(comment.comment, "Test comment")
