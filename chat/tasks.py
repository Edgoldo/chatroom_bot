from celery import shared_task

import requests
from datetime import datetime
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
from .constants import STOCK_URL, INVALID_CLOSE_VALUES

@shared_task
def get_stock_quote(stock_code, user, room):
    """!
    Task that request a csv file from STOCK_URL using stock_code parameter and
    return the financial message related

    @version 1.0.0
    @return (string) Financial messge
    """
    stock_url = STOCK_URL
    message_list = []
    channel_layer = get_channel_layer()
    try:
        stock_quote_csv = requests.get(stock_url.format(stock_code=stock_code))    
        if stock_quote_csv.status_code == 200:
            csv_lines = stock_quote_csv.text.split('\r\n')
            csv_keys = csv_lines[0].split(',')
            for line in csv_lines[1:]:
                if not line:
                    break
                csv_values = line.split(',')
                csv_dict = dict(zip(csv_keys, csv_values))

                if csv_dict['Close'] not in INVALID_CLOSE_VALUES:
                    financial_message = '{symbol} quote is ${close} per share'.format(
                        symbol=csv_dict['Symbol'],
                        close=csv_dict['Close']
                    )
                else:
                    financial_message = '{symbol} quote value is not found'.format(
                        symbol=csv_dict['Symbol']
                    )
                message_list.append(financial_message)
        else:
            message_list.append(str("Could not connect with url, status code: " +
                stock_quote_csv.status_code)
            )
    except Exception as e:
        message_list.append("Error getting stock quote value")

    for financial_message in message_list:
        text_data = str('{"comment":"%s","date":"%s","fk_user_emit":"%s","seen":false}' % (
                financial_message,
                datetime.now(),
                user
            )
        )
        async_to_sync(channel_layer.group_send)('chat_%s' % room, {
            'type': 'chat_message',
            'text_data': text_data,
        })
