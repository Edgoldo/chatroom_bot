"""
chatroom_bot chat module views
"""
## @package chat.views
#
# Views of chat application
# @date 19-07-2019
# @version 1.0
from django.core import serializers
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, ListView, CreateView

from .forms import ChatSendForm, ChatRegisterForm
from .models import Comment, ChatRoom, RoomUsers
from .tasks import get_stock_quote
from .constants import COMMAND_STOCK


class RoomView(LoginRequiredMixin, ListView):
    """!
    Class for see rooms

    @version 1.0.0
    """
    model = ChatRoom
    template_name = "room.list.html"
    paginate_by = 10

class CreateRoomView(LoginRequiredMixin, CreateView):
    """!
    Class for create rooms

    @version 1.0.0
    """
    model = ChatRoom
    template_name = "room.register.html"
    form_class = ChatRegisterForm
    success_url = reverse_lazy('chat:chat_list')


class ChatView(LoginRequiredMixin, FormView):
    """!
    Class to see chats

    @version 1.0.0
    """
    template_name = "chat.html"
    form_class = ChatSendForm

    def dispatch(self, *args, **kwargs):
        """!
        Method to dispatch view

        @param self <b>{object}</b> Class instance
        @param args <b>{object}</b> Args list
        @param kwargs <b>{object}</b> Args dict
        @return Context data
        """
        room_id = self.kwargs.get('room')
        room = ChatRoom.objects.filter(pk=room_id)

        if not room:
            return redirect('chat:chat_list')

        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """!
        Method to manage context data on view

        @param self <b>{object}</b> Class instance
        @param kwargs <b>{object}</b> Args dict
        @return Context data
        """
        room_id = self.kwargs['room']
        RoomUsers.objects.update_or_create(user=self.request.user,room_id=room_id,
            defaults={'user': self.request.user,'room_id': room_id },)
        kwargs['users'] = RoomUsers.objects.filter(room_id=room_id).all()
        kwargs['room_id'] = room_id
        return super().get_context_data(**kwargs)

class ListComment(View):
    """!
    Class to list comments

    @version 1.0.0
    """
    def get(self, request, **kwargs):
        room_id = request.GET.get('room_id')
        serialized_object = None
        if room_id is not None:
            comments = Comment.objects.filter(room_id=room_id).order_by('-date')[:50]
            comments = reversed(comments)
            serialized_object = serializers.serialize('json', comments)

        return JsonResponse(serialized_object, safe=False)

@method_decorator(csrf_exempt, name='dispatch')
class AddComment(View):
    """!
    Class to add comment

    @version 1.0.0
    """
    def post(self, request, **kwargs):
        comment = request.POST.get('comment')
        user_emit = request.POST.get('user_emit')
        room_id = request.POST.get('room_id')

        try:
            user_emit = User.objects.get(pk=user_emit)
        except Exception as e:
            print(e)
            user_emit = None
        serialized_object = None
        if user_emit is not None and room_id is not None:
            command_stock = COMMAND_STOCK
            if comment.startswith(command_stock): # and len(comment) > len(command_stock):
                get_stock_quote.apply_async((comment[len(command_stock):], user_emit.pk, room_id))
            else:
                try:
                    comment_obj = Comment()
                    comment_obj.fk_user_emit = user_emit
                    comment_obj.comment = comment
                    comment_obj.room_id = room_id
                    comment_obj.save()
                    serialized_object = serializers.serialize('json', [comment_obj])
                except Exception as e:
                    print(e)
                    serialized_object = {'error': 'Fail to send message'}

        return JsonResponse(serialized_object, safe=False)
