"""
chatroom_bot chat module Admin Configuration
"""
## @package chat.admin
#
# Admin configurations for chat module
# @date 19-07-2019
# @version 1.0
from django.contrib import admin
from .models import *

admin.site.register(ChatRoom)
admin.site.register(RoomUsers)