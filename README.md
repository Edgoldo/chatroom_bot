Chatroom_bot
============

Allows conection to a chat room and stablish communication with multiple users, activates the operation of a bot by means of commands and returns messages by response from external applications.

This instructions gonna guide you for install the project in Linux OS for development

Install version controller git:
-------------------------------

    Login like root or with super-user privileges, and introduce the next command in terminal:

    $ apt-get install git

    Logout of super-user privileges

Clone the project from repository:
----------------------------------

    To download the project source from git repository, you can make a clone with:

    $ git clone https://gitlab.com/Edgoldo/chatroom_bot.git

Create a virtual environment:
-----------------------------

    The project is developed in Python lenguage programming, you must install Python v3.4.2. The next commands should be used in terminal for PIP and Python installation. 

    Login like root or with super-user privileges

    $ apt-get install python3.4 python3-pip python3.4-dev python3-setuptools

    $ apt-get install python3-virtualenv virtualenvwrapper

    Logout of super-user privileges

    $ mkvirtualenv --python=/usr/bin/python3 chatroom_bot

Install project requirements:
-----------------------------

	First you must activate the virtual environment of chatroom_bot using next command:

	$ workon chatroom_bot

	With that command you will see the virtual environment active in your terminal:

	(chatroom_bot)$

	Get in to your project folder:

	(chatroom_bot)$ cd chatroom_bot

	And there you must install the project requirements:

	(chatroom_bot) chatroom_bot$ pip install -r requirements.txt

	Once be finished, all initial requirements are installed

Install Redis:
--------------

    To get the layer channels, is necesary install redis:

    sudo apt-get install redis-server

    And test that the client is working:

    $ redis-cli ping
    PONG

Create database and migrate the models:
---------------------------------------

    For database management at this project is been use SQLite3

    Then with actually settings file of project, you can make the migrations of models:

    (chatroom_bot) chatroom_bot$ python manage.py makemigrations

    Secure that all models are generated migrations:

    (chatroom_bot) chatroom_bot$ python manage.py makemigrations chat

    Then you can migrate to database:

    (chatroom_bot) chatroom_bot$ python manage.py migrate

Run the chatroom_bot project:
-----------------------------

    Use runserver to execute the project server:

    (chatroom_bot) chatroom_bot$ python manage.py runserver

    In your browser, introduce the base route of the project: localhost:8000

Run celery worker:
------------------

    The celery worker receive the tasks and process in asynchronus mode

    This worker represent the bot that call to external api and process the response

    To make run this bot, you must execute the following command in a new terminal:

    (chatroom_bot) chatroom_bot$ $ celery -A chatroom_bot worker -l info

Available urls:
===============

    localhost:8000/[name='home']
    localhost:8000/admin/
    localhost:8000/login [name='login']
    localhost:8000/logout [name='logout']
    localhost:8000/register [name='register']
    localhost:8000/account/password/reset/ [name='forgot']
    localhost:8000/accounts/password/done/ [name='reset_done']
    localhost:8000/account/change-pass/ [name='change_pass']
    localhost:8000/user/password/reset/<uidb64>/<token>/ [name='password_reset_confirm']
    localhost:8000/user/password/done/ [name='pass_done']
    localhost:8000/chat/ [name='chat_list']
    localhost:8000/chat/create-room [name='room_create']
    localhost:8000/chat/<int:room> [name='chat']
    localhost:8000/chat/list-comment [name='list_comment']
    localhost:8000/chat/comment [name='comment']

Project standarts:
==================

Documentation:
--------------

    The project is documented under PEP257 Docstring Conventions

    Get in to next link for his application:

    https://www.python.org/dev/peps/pep-0257/

Codification:
-------------

    The project have been codified under the style guide for coding Python PEP8

    Get in to next link for his application:

    https://www.python.org/dev/peps/pep-0008/
